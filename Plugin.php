<?php namespace PlanetaDelEste\TechMarket;

use Backend;
use Form;
use RainLab\Location\Models\Country;
use RainLab\Location\Models\State;
use System\Classes\PluginBase;

/**
 * TechMarket Plugin Information File
 */
class Plugin extends PluginBase
{
    protected static $nameCountryList;
    protected static $nameStateList;

    public $require = ['Lovata.Buddies', 'RainLab.Location'];


    /**
     * Returns information about this plugin.
     *
     * @return array
     */
    public function pluginDetails()
    {
        return [
            'name'        => 'planetadeleste.techmarket::lang.plugin.name',
            'description' => 'planetadeleste.techmarket::lang.plugin.description',
            'author'      => 'PlanetaDelEste',
            'icon'        => 'icon-picture-o'
        ];
    }

    /**
     * Registers any front-end components implemented in this plugin.
     *
     * @return array
     */
    public function registerComponents()
    {
        return [
            'PlanetaDelEste\TechMarket\Components\Banners' => 'TechMarketBanners',
            'PlanetaDelEste\TechMarket\Components\ToolBox' => 'TechMarketToolBox',
        ];
    }

    /**
     * Registers any back-end permissions used by this plugin.
     *
     * @return array
     */
    public function registerPermissions()
    {
        return [
            'planetadeleste.techmarket.banners' => [
                'tab'   => 'planetadeleste.techmarket::lang.plugin.name',
                'label' => 'planetadeleste.techmarket::lang.permission.access'
            ],
            'planetadeleste.techmarket.sizes'   => [
                'tab'   => 'planetadeleste.techmarket::lang.plugin.name',
                'label' => 'planetadeleste.techmarket::lang.permission.access_sizes'
            ],
        ];
    }

    /**
     * Registers back-end navigation items for this plugin.
     *
     * @return array
     */
    public function registerNavigation()
    {
        return [
            'techmarket' => [
                'label'       => 'planetadeleste.techmarket::lang.plugin.name',
                'url'         => Backend::url('planetadeleste/techmarket/banners'),
                'iconSvg'     => 'plugins/planetadeleste/techmarket/assets/images/icon.svg',
                'permissions' => ['planetadeleste.techmarket.*'],
                'order'       => 500,

                'sideMenu' => [
                    'banners'  => [
                        'label'       => 'planetadeleste.techmarket::lang.banners.menu_label',
                        'url'         => Backend::url('planetadeleste/techmarket/banners'),
                        'icon'        => 'icon-picture-o',
                        'permissions' => ['planetadeleste.techmarket.banners'],
                    ],
                    'sizes'    => [
                        'label'       => 'planetadeleste.techmarket::lang.sizes.menu_label',
                        'url'         => Backend::url('planetadeleste/techmarket/sizes'),
                        'icon'        => 'icon-crop',
                        'permissions' => ['planetadeleste.techmarket.sizes'],
                    ],
                    'settings' => [
                        'label' => 'planetadeleste.techmarket::lang.theme.settings',
                        'url'   => Backend::url('cms/themeoptions/update/planetadeleste-techmarket-store'),
                        'icon'  => 'icon-sliders'
                    ]
                ]
            ],
        ];
    }

    public function registerMarkupTags()
    {
        return [
            'functions' => [
                'collect'                => function ($values = null) {
                    return collect($values);
                },
                'uid'                    => function ($prefix = '', $more_entropy = false) {
                    return uniqid($prefix, $more_entropy);
                },
                'form_select_tm_country' => [$this, 'selectCountry'],
                'form_select_tm_state'   => [$this, 'selectState']
            ],
            'filters' => [
                'country' => [$this, 'getCountry'],
                'state' => [$this, 'getState'],
                'get_class' => function ($object) {
                    return get_class($object);
                },
                'class_basename' => function ($object) {
                    return class_basename($object);
                }
            ]
        ];
    }

    public function selectCountry($name, $selectedValue = null, $options = [])
    {
        return Form::select($name, self::getCountryNameList(), $selectedValue, $options);
    }

    public function selectState($name, $countryCode = null, $selectedValue = null, $options = [])
    {
        return Form::select($name, self::getStateNameList($countryCode), $selectedValue, $options);
    }

    private static function getCountryNameList()
    {
        if (self::$nameCountryList) {
            return self::$nameCountryList;
        }

        return self::$nameCountryList = Country::isEnabled()->orderBy('is_pinned', 'desc')->lists('name', 'code');
    }

    private static function getStateNameList($countryCode)
    {
        if (isset(self::$nameStateList[$countryCode])) {
            return self::$nameStateList[$countryCode];
        }

        return self::$nameStateList[$countryCode] = State::whereHas(
            'country',
            function ($query) use ($countryCode) {
                return $query->where('code', $countryCode);
            }
        )->lists('name', 'code');
    }

    public function getCountry($code, $attr = 'name')
    {
        $country = Country::where('code', $code)->first();
        return ($country) ? $country->{$attr} : null;
    }

    public function getState($code, $attr = 'name')
    {
        $state = State::where('code', $code)->first();
        return ($state) ? $state->{$attr} : null;
    }
}
