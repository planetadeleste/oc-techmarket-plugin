<?php namespace PlanetaDelEste\TechMarket\Models;

use Model;
use October\Rain\Database\Traits\Validation;

/**
 * Size Model
 *
 * @property integer             $id
 * @property \System\Models\File $image
 * @property string              $name
 * @property float               $width
 * @property float               $height
 * @property float               $percent_ratio
 * @property array               $size
 *
 */
class Size extends Model
{
    use Validation;

    /**
     * @var string The database table used by the model.
     */
    public $table = 'planetadeleste_techmarket_sizes';

    public $rules = [
        'name' => 'required|regex:/(^[0-9]+)(?:[xX])([0-9]+$)/m',
    ];

    /**
     * @var array Fillable fields
     */
    protected $fillable = ['name'];

    /**
     * @var array Relations
     */
    public $attachOne = ['image' => 'System\Models\File'];

    public function beforeValidate()
    {
        $width = post('Size._width');
        $height = post('Size._height');

        if ($width && $height) {
            $this->name = $width.'x'.$height;
        }
    }

    public function getSizeAttribute()
    {
        if(!$this->name) {
            $width = $height = '';
        } else {
            list($width, $height) = explode('x', strtolower($this->name));
        }

        return compact('width', 'height');
    }

    public function getWidthAttribute()
    {
        return floatval($this->size['width']);
    }

    public function getHeightAttribute()
    {
        return floatval($this->size['height']);
    }

    public function getPercentRatioAttribute()
    {
        return floor($this->height / ($this->width / 100));
    }
}
