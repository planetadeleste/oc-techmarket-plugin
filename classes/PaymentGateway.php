<?php
/**
 * thephonecellar.com
 * Created by alvaro.
 * User: alvaro
 * Date: 09/07/18
 * Time: 05:22 AM
 */

namespace PlanetaDelEste\TechMarket\Classes;


class PaymentGateway
{
    /**
     * @param string                                $gatewayId
     * @param \Lovata\OrdersShopaholic\Models\Order $order
     * @param \Omnipay\Common\GatewayInterface      $gateway
     *
     * @return \PlanetaDelEste\TechMarket\Classes\GatewayInterface | boolean
     */
    public static function create($gatewayId, $order, $gateway= null)
    {
        if (\File::exists(plugins_path("planetadeleste/techmarket/classes/{$gatewayId}.php"))) {
            $classname = 'PlanetaDelEste\\TechMarket\\Classes\\'.$gatewayId;
            return new $classname($order, $gateway);
        }

        return false;
    }

}