<?php


namespace PlanetaDelEste\TechMarket\Classes\Helper;

use \Cms\Classes\ComponentManager;
use System\Classes\PluginManager;

class CmsHelper
{
    /**
     * @param string $name
     *
     * @return bool
     */
    public static function hasComponent($name)
    {
        $manager = ComponentManager::instance();
        return $manager->hasComponent($name);
    }

    /**
     * @param string $namespace
     *
     * @return bool
     */
    public static function hasPlugin($namespace)
    {
        $manager = PluginManager::instance();
        if($has = $manager->hasPlugin($namespace)) {
            if($manager->isDisabled($namespace)) {
                $has = false;
            }
        }

        return $has;
    }

    /**
     * @param string                                                      $name
     * @param \Cms\Classes\PageCode|\Cms\Classes\Page|\Cms\Classes\Layout $page
     * @param null|string                                                 $alias
     *
     * @return bool|\Cms\Classes\ComponentBase
     */
    public static function getComponent($name, $page, $alias = null)
    {
        if (!static::hasComponent($name)) {
            return null;
        }

        if($alias) {
            $name = $alias;
        }

        return $page->components[$name];
    }
}
