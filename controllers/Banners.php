<?php namespace PlanetaDelEste\TechMarket\Controllers;

use BackendMenu;
use Backend\Classes\Controller;
use Flash;
use Lang;
use PlanetaDelEste\TechMarket\Models\Banner;

/**
 * Banners Back-end Controller
 */
class Banners extends Controller
{
    public $implement = [
        'Backend.Behaviors.FormController',
        'Backend.Behaviors.ListController'
    ];

    public $formConfig = 'config_form.yaml';
    public $listConfig = 'config_list.yaml';

    public $bodyClass = 'compact-container';

    public function __construct()
    {
        parent::__construct();

        $this->addCss('/plugins/planetadeleste/techmarket/assets/css/techmarket.css');
        $this->addCss('/themes/planetadeleste-techmarket-store/assets/css/font-techmarket.css');

        BackendMenu::setContext('PlanetaDelEste.TechMarket', 'techmarket', 'banners');
    }

    /**
     * Deleted checked banners.
     */
    public function index_onDelete()
    {
        if (($checkedIds = post('checked')) && is_array($checkedIds) && count($checkedIds)) {

            foreach ($checkedIds as $bannerId) {
                if (!$banner = Banner::find($bannerId)) continue;
                $banner->delete();
            }

            Flash::success(Lang::get('planetadeleste.techmarket::lang.banners.delete_selected_success'));
        }
        else {
            Flash::error(Lang::get('planetadeleste.techmarket::lang.banners.delete_selected_empty'));
        }

        return $this->listRefresh();
    }
}
